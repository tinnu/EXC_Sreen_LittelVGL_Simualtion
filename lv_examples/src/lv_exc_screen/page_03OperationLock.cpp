#include "exc_screen.h"

lv_obj_t *page_03OperationLock;

static const char * btnm_map[] = {"1", "2", "3", "4", "5", "\n",
                                  "6", "7", "8", "9", "0", "\n",
                                  "OK", "Back", "Cancel", ""};
static char secretyKey[4] = {'9', '1', '0', '0'};
static uint8_t secretyKeyFlag = 0;
static lv_obj_t *btnLabel;

static void event_handler(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_VALUE_CHANGED) {
        const char * txt = lv_btnmatrix_get_active_btn_text(obj);
        if(strstr(txt, "OK")!=NULL)
        {
            if(secretyKey[0]=='9' && secretyKey[1]=='1'
                    && secretyKey[2]=='0' && secretyKey[3]=='0')
            {
                secretyKeyFlag = 0;
                secretyKey[0]='0';
                secretyKey[1]='0';
                secretyKey[2]='0';
                secretyKey[3]='0';
                lv_label_set_text(btnLabel, "Result:Success");
            }
            else
            {
                secretyKeyFlag = 0;
                lv_label_set_text(btnLabel, "Result:Fault");
//                lvChange_switchPage(page_switch);
            }
        }
        else if(strstr(txt, "Back")!=NULL)
        {
            if(secretyKeyFlag>0) secretyKeyFlag--;
        }
        else if(strstr(txt, "Cancel")!=NULL)
        {
            secretyKeyFlag = 0;
            lvChange_switchPage(page_switch);
        }
        else{
            if(secretyKeyFlag>=0 && secretyKeyFlag<=3)
            {
                secretyKey[secretyKeyFlag] = txt[0];
                secretyKeyFlag ++;
            }
            else secretyKeyFlag++;
        }
    }
}
void lvAdd_page_03OperationLock(void)
{
    page_03OperationLock = lv_cont_create(lv_scr_act(), NULL);
    lv_obj_set_size(page_03OperationLock, 480, 272);
    lv_obj_align(page_03OperationLock, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);

    lv_obj_t * btnm1 = lv_btnmatrix_create(page_03OperationLock, NULL);
    lv_btnmatrix_set_map(btnm1, btnm_map);
    lv_btnmatrix_set_btn_width(btnm1, 10, 3);
    lv_obj_set_size(btnm1, 480, 230);
    lv_obj_align(btnm1, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, 0);
    lv_obj_set_event_cb(btnm1, event_handler);

    btnLabel = lv_label_create(page_03OperationLock, NULL);
    lv_obj_set_size(btnLabel, 480, 42);
    lv_obj_align(btnLabel, btnm1, LV_ALIGN_OUT_TOP_MID, 0, 0);
    lv_label_set_text(btnLabel, "Result:");
}
