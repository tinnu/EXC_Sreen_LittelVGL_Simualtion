#ifdef __cplusplus
extern "C" {
#endif

#ifndef _EXC_SCREEN_H_
#define _EXC_SCREEN_H_
/*********************
 *      INCLUDES
 *********************/
#include "stdint.h"
#include "lvgl/lvgl.h"

/*********************
 *      DEFINES
 *********************/
extern lv_obj_t *page_switch, *page_1, *page_2, *page_03OperationLock, *page_05Analogic, *page_06IO;

/**********************
 *      TYPEDEFS
 **********************/
//extern struct _EXCWriteDataQueue EXCWriteDataQueue;

/**********************
 * GLOBAL PROTOTYPES
 **********************/
void lvAdd_excScreen(void);
void ModbusAddWriteDataSingal(uint16_t addr, uint16_t data);
void ModbusHandleWrite(uint8_t relayTime);

void lvChange_gaugeAnim(lv_obj_t *gauge, lv_anim_value_t value);
void lv_obj_set_gaval(lv_obj_t * obj, lv_coord_t gaval);
void cb_btntoSwitch(lv_obj_t * btn, lv_event_t event);

void lvAdd_page_01firstPage(void);
void lvChange_pagemaingauge(uint16_t value1, uint16_t value2, uint16_t value3, uint16_t value4);
void lvChange_pagemainBar(uint16_t value1, uint16_t value2);

void lvAdd_page_02Promotion(void);
void lvAdd_page_03OperationLock(void);
void lvAdd_page_05Analogic();
void lvChange_page05_addPoint(uint16_t Aug, uint16_t Ail, uint16_t Bug, uint16_t Bil);
void lvAdd_page_06IO(void);

void lvAdd_pageSwitch(void);
void lvChange_switchPage(lv_obj_t *page_handle);

/**********************
 *      MACROS
 **********************/
#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
