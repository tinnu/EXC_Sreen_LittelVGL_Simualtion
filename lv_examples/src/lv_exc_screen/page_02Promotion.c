#include "exc_screen.h"

static lv_obj_t *gaugePromtion, *label_gaugePromtion;
static lv_obj_t *led_residualExcChA, *led_residualExcChB,
        *led_stepFromZeroChA, *led_stepFromZeroChB;

static void cb_btnPagePromotion2Main(lv_obj_t * btn, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED)
    {
        lvChange_switchPage(page_1);
    }
}

static void cb_btnRaiseUp(lv_obj_t * btn, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED) {
        ModbusAddWriteDataSingal(0x93, 0x01);
        ModbusAddWriteDataSingal(0x93, 0x00);
    }
}

void lvAdd_page_02Promotion(void)
{
    page_2 = lv_cont_create(lv_scr_act(), NULL);
    lv_obj_set_size(page_2, 480, 272);
    lv_obj_align(page_2, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, 0);
//    lv_page_set_scrollbar_mode(page_2, LV_SCROLLBAR_MODE_DRAG);

    //1-仪表盘
    lv_coord_t t_gaugeSize = 120;
    gaugePromtion = lv_gauge_create(page_2, NULL);
    lv_gauge_set_range(gaugePromtion,0,200);
    lv_gauge_set_critical_value(gaugePromtion, 110);
    lv_gauge_set_scale(gaugePromtion, 300, 21, 5);
    lv_obj_set_size(gaugePromtion, t_gaugeSize, t_gaugeSize);
    lv_obj_align(gaugePromtion, NULL, LV_ALIGN_IN_TOP_LEFT, 10, 10);
    lv_obj_set_click(gaugePromtion, false);
    label_gaugePromtion = lv_label_create(page_2, NULL);
    lv_label_set_text(label_gaugePromtion, "Ug\t 0.00%");
    lv_obj_align(label_gaugePromtion, gaugePromtion, LV_ALIGN_OUT_BOTTOM_MID, 0, 10);

    //2-残压起励
    lv_coord_t t_ledSize = 15;
    lv_coord_t t_ledStep = 30;

    led_residualExcChA = lv_led_create(page_2, NULL);
    lv_obj_set_size(led_residualExcChA, t_ledSize,t_ledSize);
    lv_obj_align(led_residualExcChA, gaugePromtion, LV_ALIGN_OUT_RIGHT_TOP, 120, 10);
    lv_led_off(led_residualExcChA);
    lv_obj_t *led1_label = lv_label_create(page_2, NULL);
    lv_label_set_text(led1_label, "residual ChA");
    lv_obj_align(led1_label, led_residualExcChA, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    led_stepFromZeroChA = lv_led_create(page_2, NULL);
    lv_obj_set_size(led_stepFromZeroChA, t_ledSize,t_ledSize);
    lv_obj_align(led_stepFromZeroChA, led_residualExcChA, LV_ALIGN_OUT_BOTTOM_MID, 0, t_ledStep);
    lv_led_off(led_stepFromZeroChA);
    led1_label = lv_label_create(page_2, NULL);
    lv_label_set_text(led1_label, "stepup from zero ChA");
    lv_obj_align(led1_label, led_stepFromZeroChA, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    led_residualExcChB = lv_led_create(page_2, NULL);
    lv_obj_set_size(led_residualExcChB, t_ledSize,t_ledSize);
    lv_obj_align(led_residualExcChB, led_stepFromZeroChA, LV_ALIGN_OUT_BOTTOM_MID, 0, t_ledStep);
    lv_led_off(led_residualExcChB);
    led1_label = lv_label_create(page_2, NULL);
    lv_label_set_text(led1_label, "residual ChB");
    lv_obj_align(led1_label, led_residualExcChB, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    led_stepFromZeroChB = lv_led_create(page_2, NULL);
    lv_obj_set_size(led_stepFromZeroChB, t_ledSize,t_ledSize);
    lv_obj_align(led_stepFromZeroChB, led_residualExcChB, LV_ALIGN_OUT_BOTTOM_MID, 0, t_ledStep);
    lv_led_off(led_stepFromZeroChB);
    led1_label = lv_label_create(page_2, NULL);
    lv_label_set_text(led1_label, "stepup from zero ChB");
    lv_obj_align(led1_label, led_stepFromZeroChB, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    //3-按键
    lv_obj_t * btn_residualExcChA = lv_btn_create(page_2, NULL);
    lv_obj_set_size(btn_residualExcChA, 100, 35);
    lv_obj_align(btn_residualExcChA, led_residualExcChA, LV_ALIGN_OUT_LEFT_MID, -10, 0);
    lv_obj_t *btnLabel_residualExcChA = lv_label_create(btn_residualExcChA, NULL);
    lv_label_set_text(btnLabel_residualExcChA, "ENABLE");

    btn_residualExcChA = lv_btn_create(page_2, NULL);
    lv_obj_set_size(btn_residualExcChA, 100, 35);
    lv_obj_align(btn_residualExcChA, led_stepFromZeroChA, LV_ALIGN_OUT_LEFT_MID, -10, 0);
    btnLabel_residualExcChA = lv_label_create(btn_residualExcChA, NULL);
    lv_label_set_text(btnLabel_residualExcChA, "ENABLE");

    btn_residualExcChA = lv_btn_create(page_2, NULL);
    lv_obj_set_size(btn_residualExcChA, 100, 35);
    lv_obj_align(btn_residualExcChA, led_residualExcChB, LV_ALIGN_OUT_LEFT_MID, -10, 0);
    btnLabel_residualExcChA = lv_label_create(btn_residualExcChA, NULL);
    lv_label_set_text(btnLabel_residualExcChA, "ENABLE");

    btn_residualExcChA = lv_btn_create(page_2, NULL);
    lv_obj_set_size(btn_residualExcChA, 100, 35);
    lv_obj_align(btn_residualExcChA, led_stepFromZeroChB, LV_ALIGN_OUT_LEFT_MID, -10, 0);
    btnLabel_residualExcChA = lv_label_create(btn_residualExcChA, NULL);
    lv_label_set_text(btnLabel_residualExcChA, "ENABLE");

    btn_residualExcChA = lv_btn_create(page_2, NULL);
    lv_obj_set_size(btn_residualExcChA, 100, 35);
    lv_obj_align(btn_residualExcChA, page_2, LV_ALIGN_IN_BOTTOM_LEFT, 10, -10);
    lv_obj_set_event_cb(btn_residualExcChA, cb_btnRaiseUp);
    btnLabel_residualExcChA = lv_label_create(btn_residualExcChA, NULL);
    lv_label_set_text(btnLabel_residualExcChA, "EXC UP");

    //4-返回按钮
    lv_obj_t * btn_toSwitch = lv_btn_create(page_2, NULL);     //Add a button the current screen
    lv_obj_set_size(btn_toSwitch, 100, 35);                          //Set its size
    lv_obj_set_event_cb(btn_toSwitch, cb_btntoSwitch);                 //Assign a callback to the button
    lv_obj_align(btn_toSwitch, page_2, LV_ALIGN_IN_BOTTOM_RIGHT , -10, -10);
    lv_obj_t *btnLabel_toSwitch = lv_label_create(btn_toSwitch, NULL);
    lv_label_set_text(btnLabel_toSwitch, "Swich Page");

    lv_obj_t * btn_PagePromotion2Main = lv_btn_create(page_2, NULL);
    lv_obj_set_size(btn_PagePromotion2Main, 100, 35);
    lv_obj_set_event_cb(btn_PagePromotion2Main, cb_btnPagePromotion2Main);
    lv_obj_align(btn_PagePromotion2Main, btn_toSwitch, LV_ALIGN_OUT_LEFT_MID, -10, 0);
    lv_obj_t *btnLabel_PagePromotion2Main = lv_label_create(btn_PagePromotion2Main, NULL);
    lv_label_set_text(btnLabel_PagePromotion2Main, "Main");

}
void lvChange_pagePromotionGauge(uint16_t value1)
{
    static uint16_t t1=0;
    char t_char[30];

    if(t1 != value1)
    {
        sprintf(t_char, "Ug\t %d.%d %%", value1/100, value1%100);
        lv_label_set_text(label_gaugePromtion, t_char);
        lvChange_gaugeAnim(gaugePromtion, (double)value1/100);
    }
    t1=value1;
}

void lvChange_pagePromotionLed(bool b1, bool b2, bool b3, bool b4)
{
    if(b1) lv_led_on(led_residualExcChA);	//残压
    else lv_led_off(led_residualExcChA);
    if(b2) lv_led_on(led_stepFromZeroChA);	//零升
    else lv_led_off(led_stepFromZeroChA);
    if(b3) lv_led_on(led_residualExcChB);
    else lv_led_off(led_residualExcChB);
    if(b4) lv_led_on(led_stepFromZeroChB);
    else lv_led_off(led_stepFromZeroChB);
}
