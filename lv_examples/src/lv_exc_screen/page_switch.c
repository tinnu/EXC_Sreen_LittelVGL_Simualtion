#include "exc_screen.h"

static lv_obj_t *btn_01firstPage, *btn_02Promotion, *btn_03OperationLock,
                *btn_04alarmMsg, *btn_05analog, *btn_06IO,
                *btn_07runningMode, *btn_08relayTest, *btn_09systemSetting,
                *btn_10language, *btn_11companyIntro, *btn_12contactWay;

static void cb_btnPageChange(lv_obj_t * btn, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED)
    {
        if(btn == btn_01firstPage) lvChange_switchPage(page_1);
        else if(btn == btn_02Promotion) lvChange_switchPage(page_2);
        else if(btn == btn_03OperationLock) lvChange_switchPage(page_03OperationLock);
        else if(btn == btn_05analog) lvChange_switchPage(page_05Analogic);
        else if(btn == btn_06IO) lvChange_switchPage(page_06IO);
    }
}

void lvAdd_pageSwitch(void)
{
    lv_coord_t t_btnWidth = 200, t_btnHeight = 35, t_alignStep = -42, t_alignStart = -10;
    page_switch = lv_page_create(lv_scr_act(), NULL);
    lv_obj_set_size(page_switch, 480, 272);
    lv_obj_align(page_switch, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, 0);
    lv_page_set_scrollbar_mode(page_switch, LV_SCROLLBAR_MODE_DRAG);

    btn_01firstPage = lv_btn_create(page_switch, NULL);
    lv_obj_set_size(btn_01firstPage, t_btnWidth, t_btnHeight);
    lv_obj_set_event_cb(btn_01firstPage, cb_btnPageChange);
    lv_obj_align(btn_01firstPage, page_switch, LV_ALIGN_IN_BOTTOM_LEFT, 20, t_alignStart);
    lv_obj_t *btnLabel = lv_label_create(btn_01firstPage, NULL);
    lv_label_set_text(btnLabel, "First Page");

    btn_02Promotion = lv_btn_create(page_switch, NULL);
    lv_obj_set_size(btn_02Promotion, t_btnWidth, t_btnHeight);
    lv_obj_set_event_cb(btn_02Promotion, cb_btnPageChange);
    lv_obj_align(btn_02Promotion, page_switch, LV_ALIGN_IN_BOTTOM_LEFT, 20, t_alignStart+t_alignStep);
    btnLabel = lv_label_create(btn_02Promotion, NULL);
    lv_label_set_text(btnLabel, "Promotion");

    btn_03OperationLock = lv_btn_create(page_switch, NULL);
    lv_obj_set_size(btn_03OperationLock, t_btnWidth, t_btnHeight);
    lv_obj_set_event_cb(btn_03OperationLock, cb_btnPageChange);
    lv_obj_align(btn_03OperationLock, page_switch, LV_ALIGN_IN_BOTTOM_LEFT, 20, t_alignStart+t_alignStep*2);
    btnLabel = lv_label_create(btn_03OperationLock, NULL);
    lv_label_set_text(btnLabel, "Operation Lock");

    btn_04alarmMsg = lv_btn_create(page_switch, NULL);
    lv_obj_set_size(btn_04alarmMsg, t_btnWidth, t_btnHeight);
    lv_obj_set_event_cb(btn_04alarmMsg, cb_btnPageChange);
    lv_obj_align(btn_04alarmMsg, page_switch, LV_ALIGN_IN_BOTTOM_LEFT, 20, t_alignStart+t_alignStep*3);
    btnLabel = lv_label_create(btn_04alarmMsg, NULL);
    lv_label_set_text(btnLabel, "Alarm");

    btn_05analog = lv_btn_create(page_switch, NULL);
    lv_obj_set_size(btn_05analog, t_btnWidth, t_btnHeight);
    lv_obj_set_event_cb(btn_05analog, cb_btnPageChange);
    lv_obj_align(btn_05analog, page_switch, LV_ALIGN_IN_BOTTOM_LEFT, 20, t_alignStart+t_alignStep*4);
    btnLabel = lv_label_create(btn_05analog, NULL);
    lv_label_set_text(btnLabel, "Analogic Moniter");

    btn_06IO = lv_btn_create(page_switch, NULL);
    lv_obj_set_size(btn_06IO, t_btnWidth, t_btnHeight);
    lv_obj_set_event_cb(btn_06IO, cb_btnPageChange);
    lv_obj_align(btn_06IO, page_switch, LV_ALIGN_IN_BOTTOM_LEFT, 20, t_alignStart+t_alignStep*5);
    btnLabel = lv_label_create(btn_06IO, NULL);
    lv_label_set_text(btnLabel, "IO Moniter");

    btn_07runningMode = lv_btn_create(page_switch, NULL);
    lv_obj_set_size(btn_07runningMode, t_btnWidth, t_btnHeight);
    lv_obj_set_event_cb(btn_07runningMode, cb_btnPageChange);
    lv_obj_align(btn_07runningMode, page_switch, LV_ALIGN_IN_BOTTOM_RIGHT, -20, t_alignStart+t_alignStep*5);
    btnLabel = lv_label_create(btn_07runningMode, NULL);
    lv_label_set_text(btnLabel, "Running Mode");

    btn_08relayTest = lv_btn_create(page_switch, NULL);
    lv_obj_set_size(btn_08relayTest, t_btnWidth, t_btnHeight);
    lv_obj_set_event_cb(btn_08relayTest, cb_btnPageChange);
    lv_obj_align(btn_08relayTest, page_switch, LV_ALIGN_IN_BOTTOM_RIGHT, -20, t_alignStart+t_alignStep*4);
    btnLabel = lv_label_create(btn_08relayTest, NULL);
    lv_label_set_text(btnLabel, "Relay Test");

    btn_09systemSetting = lv_btn_create(page_switch, NULL);
    lv_obj_set_size(btn_09systemSetting, t_btnWidth, t_btnHeight);
    lv_obj_set_event_cb(btn_09systemSetting, cb_btnPageChange);
    lv_obj_align(btn_09systemSetting, page_switch, LV_ALIGN_IN_BOTTOM_RIGHT, -20, t_alignStart+t_alignStep*3);
    btnLabel = lv_label_create(btn_09systemSetting, NULL);
    lv_label_set_text(btnLabel, "System Setting");

    btn_10language = lv_btn_create(page_switch, NULL);
    lv_obj_set_size(btn_10language, t_btnWidth, t_btnHeight);
    lv_obj_set_event_cb(btn_10language, cb_btnPageChange);
    lv_obj_align(btn_10language, page_switch, LV_ALIGN_IN_BOTTOM_RIGHT, -20, t_alignStart+t_alignStep*2);
    btnLabel = lv_label_create(btn_10language, NULL);
    lv_label_set_text(btnLabel, "Language");

    btn_11companyIntro = lv_btn_create(page_switch, NULL);
    lv_obj_set_size(btn_11companyIntro, t_btnWidth, t_btnHeight);
    lv_obj_set_event_cb(btn_11companyIntro, cb_btnPageChange);
    lv_obj_align(btn_11companyIntro, page_switch, LV_ALIGN_IN_BOTTOM_RIGHT, -20, t_alignStart+t_alignStep);
    btnLabel = lv_label_create(btn_11companyIntro, NULL);
    lv_label_set_text(btnLabel, "Company Introduction");

    btn_12contactWay = lv_btn_create(page_switch, NULL);
    lv_obj_set_size(btn_12contactWay, t_btnWidth, t_btnHeight);
    lv_obj_set_event_cb(btn_12contactWay, cb_btnPageChange);
    lv_obj_align(btn_12contactWay, page_switch, LV_ALIGN_IN_BOTTOM_RIGHT, -20, t_alignStart);
    btnLabel = lv_label_create(btn_12contactWay, NULL);
    lv_label_set_text(btnLabel, "Contact Way");

}

void lvChange_switchPage(lv_obj_t *page_handle)
{
    lv_obj_set_hidden(page_1, true);
    lv_obj_set_hidden(page_2, true);
    lv_obj_set_hidden(page_03OperationLock, true);
    lv_obj_set_hidden(page_05Analogic, true);
    lv_obj_set_hidden(page_06IO, true);
    lv_obj_set_hidden(page_switch, true);
    lv_obj_set_hidden(page_handle, false);
}
