#include "exc_screen.h"

lv_obj_t *page_06IO;
static lv_obj_t *page_IOChA, *page_IOChB, *page_IOChS;
static lv_obj_t *btn_A, *btn_B, *btn_S;
static uint32_t led_IOChA[16], led_IOChB[16], led_IOChS[16];

static void lvChange_ioPage(lv_obj_t *page_handle)
{
    lv_obj_set_hidden(page_IOChA, true);
    lv_obj_set_hidden(page_IOChB, true);
    lv_obj_set_hidden(page_IOChS, true);
    lv_obj_set_hidden(page_handle, false);
}
static void cb_btntoIOPageA(lv_obj_t * btn, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED) {
        lvChange_ioPage(page_IOChA);
        lv_obj_clear_state(btn_B, LV_STATE_CHECKED);
        lv_obj_clear_state(btn_S, LV_STATE_CHECKED);
        lv_obj_add_state(btn, LV_STATE_CHECKED);
    }
}
static void cb_btntoIOPageB(lv_obj_t * btn, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED) {
        lvChange_ioPage(page_IOChB);
        lv_obj_clear_state(btn_A, LV_STATE_CHECKED);
        lv_obj_clear_state(btn_S, LV_STATE_CHECKED);
        lv_obj_add_state(btn, LV_STATE_CHECKED);
    }
}
static void cb_btntoIOPageS(lv_obj_t * btn, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED) {
        lvChange_ioPage(page_IOChS);
        lv_obj_clear_state(btn_B, LV_STATE_CHECKED);
        lv_obj_clear_state(btn_A, LV_STATE_CHECKED);
        lv_obj_add_state(btn, LV_STATE_CHECKED);
    }
}

static void lvAdd_page_IOCh(lv_obj_t *page_IOCh, uint32_t *led_IOCh)
{
    page_IOCh = lv_cont_create(page_06IO, NULL);
    lv_obj_set_size(page_IOCh, 480, 272-55);
    lv_obj_align(page_IOCh, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);

    //2-状态指示灯
    lv_coord_t t_ledSize = 22;
    lv_coord_t t_ledStep = 33;
    lv_obj_t *t_ledObj;

    t_ledObj = lv_led_create(page_IOCh, NULL);
    led_IOCh[0] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOCh, LV_ALIGN_IN_TOP_LEFT, 10, 15);
    lv_led_off(t_ledObj);
    lv_obj_t *led1_label = lv_label_create(page_IOCh, NULL);
    lv_label_set_text(led1_label, "preset exc");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOCh, NULL);
    led_IOCh[1] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOCh, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOCh, NULL);
    lv_label_set_text(led1_label, "zeroset exc");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOCh, NULL);
    led_IOCh[2] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOCh, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep*2);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOCh, NULL);
    lv_label_set_text(led1_label, "cha follow");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOCh, NULL);
    led_IOCh[3] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOCh, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep*3);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOCh, NULL);
    lv_label_set_text(led1_label, "us follow");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);
//*
    t_ledObj = lv_led_create(page_IOCh, NULL);
    led_IOCh[4] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOCh, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep*4);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOCh, NULL);
    lv_label_set_text(led1_label, "INV");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOCh, NULL);
    led_IOCh[5] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOCh, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep*5);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOCh, NULL);
    lv_label_set_text(led1_label, "Constant IL");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    //2
    t_ledObj = lv_led_create(page_IOCh, NULL);
    led_IOCh[6] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOCh, LV_ALIGN_IN_TOP_LEFT, 170, 15);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOCh, NULL);
    lv_label_set_text(led1_label, "R631");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOCh, NULL);
    led_IOCh[7] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOCh, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOCh, NULL);
    lv_label_set_text(led1_label, "R632");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOCh, NULL);
    led_IOCh[8] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOCh, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep*2);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOCh, NULL);
    lv_label_set_text(led1_label, "Constant Q");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOCh, NULL);
    led_IOCh[9] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOCh, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep*3);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOCh, NULL);
    lv_label_set_text(led1_label, "Constant PF");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOCh, NULL);
    led_IOCh[10] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOCh, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep*4);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOCh, NULL);
    lv_label_set_text(led1_label, "PSS OUTPUT");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOCh, NULL);
    led_IOCh[11] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOCh, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep*5);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOCh, NULL);
    lv_label_set_text(led1_label, "Over EXC Enable");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    //3
    t_ledObj = lv_led_create(page_IOCh, NULL);
    led_IOCh[12] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOCh, LV_ALIGN_IN_TOP_LEFT, 320, 15);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOCh, NULL);
    lv_label_set_text(led1_label, "Over EXC Act");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOCh, NULL);
    led_IOCh[13] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOCh, LV_ALIGN_IN_TOP_LEFT, 320, 15+t_ledStep);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOCh, NULL);
    lv_label_set_text(led1_label, "Current Limit");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOCh, NULL);
    led_IOCh[14] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOCh, LV_ALIGN_IN_TOP_LEFT, 320, 15+t_ledStep*2);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOCh, NULL);
    lv_label_set_text(led1_label, "Low EXC Limit");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);
    //*/

    t_ledObj = lv_led_create(page_IOCh, NULL);
    led_IOCh[15] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOCh, LV_ALIGN_IN_TOP_LEFT, 320, 15+t_ledStep*3);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOCh, NULL);
    lv_label_set_text(led1_label, "VF limit");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);
}
static void lvAdd_page_IOChA()
{
    page_IOChA = lv_cont_create(page_06IO, NULL);
    lv_obj_set_size(page_IOChA, 480, 272-55);
    lv_obj_align(page_IOChA, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);

    //2-状态指示灯
    lv_coord_t t_ledSize = 22;
    lv_coord_t t_ledStep = 33;
    lv_obj_t *t_ledObj;

    t_ledObj = lv_led_create(page_IOChA, NULL);
    led_IOChA[0] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChA, LV_ALIGN_IN_TOP_LEFT, 10, 15);
    lv_led_off(t_ledObj);
    lv_obj_t *led1_label = lv_label_create(page_IOChA, NULL);
    lv_label_set_text(led1_label, "preset exc");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChA, NULL);
    led_IOChA[1] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChA, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChA, NULL);
    lv_label_set_text(led1_label, "zeroset exc");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChA, NULL);
    led_IOChA[2] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChA, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep*2);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChA, NULL);
    lv_label_set_text(led1_label, "cha follow");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChA, NULL);
    led_IOChA[3] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChA, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep*3);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChA, NULL);
    lv_label_set_text(led1_label, "us follow");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);
//*
    t_ledObj = lv_led_create(page_IOChA, NULL);
    led_IOChA[4] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChA, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep*4);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChA, NULL);
    lv_label_set_text(led1_label, "INV");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChA, NULL);
    led_IOChA[5] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChA, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep*5);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChA, NULL);
    lv_label_set_text(led1_label, "Constant IL");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    //2
    t_ledObj = lv_led_create(page_IOChA, NULL);
    led_IOChA[6] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChA, LV_ALIGN_IN_TOP_LEFT, 170, 15);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChA, NULL);
    lv_label_set_text(led1_label, "R631");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChA, NULL);
    led_IOChA[7] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChA, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChA, NULL);
    lv_label_set_text(led1_label, "R632");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChA, NULL);
    led_IOChA[8] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChA, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep*2);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChA, NULL);
    lv_label_set_text(led1_label, "Constant Q");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChA, NULL);
    led_IOChA[9] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChA, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep*3);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChA, NULL);
    lv_label_set_text(led1_label, "Constant PF");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChA, NULL);
    led_IOChA[10] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChA, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep*4);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChA, NULL);
    lv_label_set_text(led1_label, "PSS OUTPUT");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChA, NULL);
    led_IOChA[11] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChA, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep*5);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChA, NULL);
    lv_label_set_text(led1_label, "Over EXC Enable");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    //3
    t_ledObj = lv_led_create(page_IOChA, NULL);
    led_IOChA[12] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChA, LV_ALIGN_IN_TOP_LEFT, 320, 15);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChA, NULL);
    lv_label_set_text(led1_label, "Over EXC Act");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChA, NULL);
    led_IOChA[13] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChA, LV_ALIGN_IN_TOP_LEFT, 320, 15+t_ledStep);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChA, NULL);
    lv_label_set_text(led1_label, "Current Limit");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChA, NULL);
    led_IOChA[14] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChA, LV_ALIGN_IN_TOP_LEFT, 320, 15+t_ledStep*2);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChA, NULL);
    lv_label_set_text(led1_label, "Low EXC Limit");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);
    //*/

    t_ledObj = lv_led_create(page_IOChA, NULL);
    led_IOChA[15] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChA, LV_ALIGN_IN_TOP_LEFT, 320, 15+t_ledStep*3);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChA, NULL);
    lv_label_set_text(led1_label, "VF limit");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);
}
static void lvAdd_page_IOChB()
{
    page_IOChB = lv_cont_create(page_06IO, NULL);
    lv_obj_set_size(page_IOChB, 480, 272-55);
    lv_obj_align(page_IOChB, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);

    //2-状态指示灯
    lv_coord_t t_ledSize = 22;
    lv_coord_t t_ledStep = 33;
    lv_obj_t *t_ledObj;

    t_ledObj = lv_led_create(page_IOChB, NULL);
    led_IOChB[0] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChB, LV_ALIGN_IN_TOP_LEFT, 10, 15);
    lv_led_off(t_ledObj);
    lv_obj_t *led1_label = lv_label_create(page_IOChB, NULL);
    lv_label_set_text(led1_label, "preset exc");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChB, NULL);
    led_IOChB[1] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChB, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChB, NULL);
    lv_label_set_text(led1_label, "zeroset exc");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);
//*

    t_ledObj = lv_led_create(page_IOChB, NULL);
    led_IOChB[2] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChB, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep*2);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChB, NULL);
    lv_label_set_text(led1_label, "cha follow");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChB, NULL);
    led_IOChB[3] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChB, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep*3);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChB, NULL);
    lv_label_set_text(led1_label, "us follow");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);
//*
    t_ledObj = lv_led_create(page_IOChB, NULL);
    led_IOChB[4] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChB, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep*4);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChB, NULL);
    lv_label_set_text(led1_label, "INV");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChB, NULL);
    led_IOChB[5] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChB, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep*5);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChB, NULL);
    lv_label_set_text(led1_label, "Constant IL");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    //2
    t_ledObj = lv_led_create(page_IOChB, NULL);
    led_IOChB[6] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChB, LV_ALIGN_IN_TOP_LEFT, 170, 15);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChB, NULL);
    lv_label_set_text(led1_label, "R631");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChB, NULL);
    led_IOChB[7] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChB, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChB, NULL);
    lv_label_set_text(led1_label, "R632");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChB, NULL);
    led_IOChB[8] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChB, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep*2);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChB, NULL);
    lv_label_set_text(led1_label, "Constant Q");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChB, NULL);
    led_IOChB[9] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChB, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep*3);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChB, NULL);
    lv_label_set_text(led1_label, "Constant PF");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChB, NULL);
    led_IOChB[10] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChB, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep*4);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChB, NULL);
    lv_label_set_text(led1_label, "PSS OUTPUT");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChB, NULL);
    led_IOChB[11] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChB, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep*5);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChB, NULL);
    lv_label_set_text(led1_label, "Over EXC Enable");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    //3
    t_ledObj = lv_led_create(page_IOChB, NULL);
    led_IOChB[12] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChB, LV_ALIGN_IN_TOP_LEFT, 320, 15);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChB, NULL);
    lv_label_set_text(led1_label, "Over EXC Act");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChB, NULL);
    led_IOChB[13] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChB, LV_ALIGN_IN_TOP_LEFT, 320, 15+t_ledStep);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChB, NULL);
    lv_label_set_text(led1_label, "Current Limit");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChB, NULL);
    led_IOChB[14] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChB, LV_ALIGN_IN_TOP_LEFT, 320, 15+t_ledStep*2);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChB, NULL);
    lv_label_set_text(led1_label, "Low EXC Limit");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);
    //*/

    t_ledObj = lv_led_create(page_IOChB, NULL);
    led_IOChB[15] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChB, LV_ALIGN_IN_TOP_LEFT, 320, 15+t_ledStep*3);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChB, NULL);
    lv_label_set_text(led1_label, "VF limit");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);
//*/
}
static void lvAdd_page_IOChS()
{
    page_IOChS = lv_cont_create(page_06IO, NULL);
    lv_obj_set_size(page_IOChS, 480, 272-55);
    lv_obj_align(page_IOChS, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);

    //2-状态指示灯
    lv_coord_t t_ledSize = 22;
    lv_coord_t t_ledStep = 33;
    lv_obj_t *t_ledObj;

    t_ledObj = lv_led_create(page_IOChS, NULL);
    led_IOChS[0] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChS, LV_ALIGN_IN_TOP_LEFT, 10, 15);
    lv_led_off(t_ledObj);
    lv_obj_t *led1_label = lv_label_create(page_IOChS, NULL);
    lv_label_set_text(led1_label, "normal mode");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChS, NULL);
    led_IOChS[1] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChS, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChS, NULL);
    lv_label_set_text(led1_label, "zeroset exc");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChS, NULL);
    led_IOChS[2] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChS, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep*2);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChS, NULL);
    lv_label_set_text(led1_label, "Constant Singal");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChS, NULL);
    led_IOChS[3] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChS, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep*3);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChS, NULL);
    lv_label_set_text(led1_label, "us follow");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChS, NULL);
    led_IOChS[4] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChS, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep*4);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChS, NULL);
    lv_label_set_text(led1_label, "INV");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChS, NULL);
    led_IOChS[5] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChS, LV_ALIGN_IN_TOP_LEFT, 10, 15+t_ledStep*5);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChS, NULL);
    lv_label_set_text(led1_label, "Constant IL");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    //2
    t_ledObj = lv_led_create(page_IOChS, NULL);
    led_IOChS[6] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChS, LV_ALIGN_IN_TOP_LEFT, 170, 15);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChS, NULL);
    lv_label_set_text(led1_label, "R631");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChS, NULL);
    led_IOChS[7] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChS, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChS, NULL);
    lv_label_set_text(led1_label, "R632");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChS, NULL);
    led_IOChS[8] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChS, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep*2);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChS, NULL);
    lv_label_set_text(led1_label, "Constant Q");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChS, NULL);
    led_IOChS[9] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChS, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep*3);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChS, NULL);
    lv_label_set_text(led1_label, "Constant PF");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChS, NULL);
    led_IOChS[10] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChS, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep*4);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChS, NULL);
    lv_label_set_text(led1_label, "PSS OUTPUT");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChS, NULL);
    led_IOChS[11] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChS, LV_ALIGN_IN_TOP_LEFT, 170, 15+t_ledStep*5);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChS, NULL);
    lv_label_set_text(led1_label, "Over EXC Enable");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    //3
    t_ledObj = lv_led_create(page_IOChS, NULL);
    led_IOChS[12] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChS, LV_ALIGN_IN_TOP_LEFT, 320, 15);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChS, NULL);
    lv_label_set_text(led1_label, "Over EXC Act");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChS, NULL);
    led_IOChS[13] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChS, LV_ALIGN_IN_TOP_LEFT, 320, 15+t_ledStep);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChS, NULL);
    lv_label_set_text(led1_label, "Current Limit");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    t_ledObj = lv_led_create(page_IOChS, NULL);
    led_IOChS[14] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChS, LV_ALIGN_IN_TOP_LEFT, 320, 15+t_ledStep*2);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChS, NULL);
    lv_label_set_text(led1_label, "Low EXC Limit");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);
    //*/

    t_ledObj = lv_led_create(page_IOChS, NULL);
    led_IOChS[15] = (uint32_t)t_ledObj;
    lv_obj_set_size(t_ledObj, t_ledSize, t_ledSize);
    lv_obj_align(t_ledObj, page_IOChS, LV_ALIGN_IN_TOP_LEFT, 320, 15+t_ledStep*3);
    lv_led_off(t_ledObj);
    led1_label = lv_label_create(page_IOChS, NULL);
    lv_label_set_text(led1_label, "VF limit");
    lv_obj_align(led1_label, t_ledObj, LV_ALIGN_OUT_RIGHT_MID, 10, 0);
}

void lvAdd_page_06IO(void)
{
    page_06IO = lv_cont_create(lv_scr_act(), NULL);
    lv_obj_set_size(page_06IO, 480, 272);
    lv_obj_align(page_06IO, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);

//    lvAdd_page_IOChA(page_IOChA, led_IOChA);
    lvAdd_page_IOChA();
//    lvAdd_page_IOCh(page_IOChB, led_IOChB);
    lvAdd_page_IOChB();
    lvAdd_page_IOChS();

    lvChange_ioPage(page_IOChA);


    btn_A = lv_btn_create(page_06IO, NULL);
    lv_obj_set_size(btn_A, 100, 35);
    lv_obj_align(btn_A, page_06IO, LV_ALIGN_IN_BOTTOM_LEFT, 10, -12);
    lv_obj_set_event_cb(btn_A, cb_btntoIOPageA);
    lv_obj_t *t_btnLabel = lv_label_create(btn_A, NULL);
    lv_label_set_text(t_btnLabel, "A");
    lv_btn_set_checkable(btn_A, true);
    lv_btn_toggle(btn_A);

    btn_B = lv_btn_create(page_06IO, NULL);
    lv_obj_set_size(btn_B, 100, 35);
    lv_obj_align(btn_B, page_06IO, LV_ALIGN_IN_BOTTOM_LEFT, 120, -12);
    lv_obj_set_event_cb(btn_B, cb_btntoIOPageB);
    t_btnLabel = lv_label_create(btn_B, NULL);
    lv_label_set_text(t_btnLabel, "B");
    lv_btn_set_checkable(btn_B, true);

    btn_S = lv_btn_create(page_06IO, NULL);
    lv_obj_set_size(btn_S, 100, 35);
    lv_obj_align(btn_S, page_06IO, LV_ALIGN_IN_BOTTOM_LEFT, 230, -12);
    lv_obj_set_event_cb(btn_S, cb_btntoIOPageS);
    t_btnLabel = lv_label_create(btn_S, NULL);
    lv_label_set_text(t_btnLabel, "S");
    lv_btn_set_checkable(btn_S, true);

    lv_obj_t * btn_toSwitch = lv_btn_create(page_06IO, NULL);     //Add a button the current screen
    lv_obj_set_size(btn_toSwitch, 100, 35);                          //Set its size
    lv_obj_set_event_cb(btn_toSwitch, cb_btntoSwitch);                 //Assign a callback to the button
    lv_obj_align(btn_toSwitch, page_06IO, LV_ALIGN_IN_BOTTOM_RIGHT, -10, -12);
    lv_obj_t *btnLabel_toSwitch = lv_label_create(btn_toSwitch, NULL);
    lv_label_set_text(btnLabel_toSwitch, "BACK");
}

void lvChange_page6IOBar(uint16_t value1, uint16_t value2)
{

}
