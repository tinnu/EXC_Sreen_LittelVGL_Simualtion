#include "exc_screen.h"
#include "stdio.h"
#include "math.h"

lv_obj_t *page_05Analogic;
static lv_obj_t *page_AnaChA, *page_AnaChB;
static lv_obj_t *btn_A, *btn_B;
static lv_obj_t *label_Aug, *label_Ail, *label_Bug, *label_Bil;

static lv_obj_t *chart_A, *chart_B;
static lv_chart_series_t *serA_ug, * serA_il, *serB_ug, * serB_il;

//static void lvChange_anaPage(lv_obj_t *page_handle)
//{
//    lv_obj_set_hidden(page_AnaChA, true);
//    lv_obj_set_hidden(page_AnaChB, true);
//    lv_obj_set_hidden(page_handle, false);
//}
static void cb_btntoAnaPageA(lv_obj_t * btn, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED) {
        lv_obj_clear_state(btn_B, LV_STATE_CHECKED);
        lv_obj_add_state(btn_A, LV_STATE_CHECKED);
        lv_obj_set_hidden(page_AnaChB, true);
        lv_obj_set_hidden(page_AnaChA, false);
    }
}
static void cb_btntoAnaPageB(lv_obj_t * btn, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED) {
        lv_obj_clear_state(btn_A, LV_STATE_CHECKED);
        lv_obj_add_state(btn_B, LV_STATE_CHECKED);
        lv_obj_set_hidden(page_AnaChA, true);
        lv_obj_set_hidden(page_AnaChB, false);
    }
}
static void lvAdd_page_AnaChA()
{
    page_AnaChA = lv_cont_create(page_05Analogic, NULL);
    lv_obj_set_size(page_AnaChA, 480, 272-55);
    lv_obj_align(page_AnaChA, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);

    lv_obj_t *t_label = lv_label_create(page_05Analogic, NULL);
    lv_label_set_text(t_label, "Ug");
    lv_obj_align(t_label, NULL, LV_ALIGN_IN_TOP_LEFT, 10, 10);
    label_Aug = lv_label_create(page_05Analogic, NULL);
    lv_label_set_text(label_Aug, "0.00");
    lv_obj_align(label_Aug, t_label, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 0);

    t_label = lv_label_create(page_05Analogic, NULL);
    lv_label_set_text(t_label, "IL");
    lv_obj_align(t_label, label_Aug, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 15);
    label_Ail = lv_label_create(page_05Analogic, NULL);
    lv_label_set_text(label_Ail, "0.00");
    lv_obj_align(label_Ail, t_label, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 0);

    chart_A = lv_chart_create(page_AnaChA, NULL);
    lv_obj_set_size(chart_A, 400, 272-55);
    lv_obj_align(chart_A, page_AnaChA, LV_ALIGN_IN_RIGHT_MID, 0, 0);
    lv_chart_set_type(chart_A, LV_CHART_TYPE_LINE);
    lv_chart_set_range(chart_A, 0, 120);
    lv_chart_set_point_count(chart_A, 30);

    serA_ug = lv_chart_add_series(chart_A, LV_COLOR_YELLOW);
    lv_chart_set_next(chart_A, serA_ug, 10);
    lv_chart_set_next(chart_A, serA_ug, 50);
    serA_il = lv_chart_add_series(chart_A, LV_COLOR_ORANGE);
    lv_chart_set_next(chart_A, serA_il, 20);
    lv_chart_set_next(chart_A, serA_il, 50);
}

static void lvAdd_page_AnaChB()
{
    page_AnaChB = lv_cont_create(page_05Analogic, NULL);
    lv_obj_set_size(page_AnaChB, 480, 272-55);
    lv_obj_align(page_AnaChB, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);

    lv_obj_t *t_label = lv_label_create(page_05Analogic, NULL);
    lv_label_set_text(t_label, "Ug");
    lv_obj_align(t_label, NULL, LV_ALIGN_IN_TOP_LEFT, 10, 10);
    label_Bug = lv_label_create(page_05Analogic, NULL);
    lv_label_set_text(label_Bug, "0.00");
    lv_obj_align(label_Bug, t_label, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 0);

    t_label = lv_label_create(page_05Analogic, NULL);
    lv_label_set_text(t_label, "IL");
    lv_obj_align(t_label, label_Bug, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 15);
    label_Bil = lv_label_create(page_05Analogic, NULL);
    lv_label_set_text(label_Bil, "0.00");
    lv_obj_align(label_Bil, t_label, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 0);

    chart_B = lv_chart_create(page_AnaChB, NULL);
    lv_obj_set_size(chart_B, 400, 272-55);
    lv_obj_align(chart_B, page_AnaChB, LV_ALIGN_IN_RIGHT_MID, 0, 0);
    lv_chart_set_type(chart_B, LV_CHART_TYPE_LINE);
    lv_chart_set_range(chart_B, 0, 120);
    lv_chart_set_point_count(chart_B, 30);

    serB_ug = lv_chart_add_series(chart_B, LV_COLOR_YELLOW);
    lv_chart_set_next(chart_B, serB_ug, 10);
    lv_chart_set_next(chart_B, serB_ug, 50);
    serB_il = lv_chart_add_series(chart_B, LV_COLOR_ORANGE);
    lv_chart_set_next(chart_B, serB_il, 20);
    lv_chart_set_next(chart_B, serB_il, 50);
}

void lvAdd_page_05Analogic()
{
    page_05Analogic = lv_cont_create(lv_scr_act(), NULL);
    lv_obj_set_size(page_05Analogic, 480, 272);
    lv_obj_align(page_05Analogic, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);

    lvAdd_page_AnaChA();
    lvAdd_page_AnaChB();
    lv_obj_set_hidden(page_AnaChB, true);

    btn_A = lv_btn_create(page_05Analogic, NULL);
    lv_obj_set_size(btn_A, 100, 35);
    lv_obj_align(btn_A, page_05Analogic, LV_ALIGN_IN_BOTTOM_LEFT, 10, -12);
    lv_obj_set_event_cb(btn_A, cb_btntoAnaPageA);
    lv_obj_t *t_btnLabel = lv_label_create(btn_A, NULL);
    lv_label_set_text(t_btnLabel, "A");
    lv_btn_set_checkable(btn_A, true);
    lv_btn_toggle(btn_A);

    btn_B = lv_btn_create(page_05Analogic, NULL);
    lv_obj_set_size(btn_B, 100, 35);
    lv_obj_align(btn_B, page_05Analogic, LV_ALIGN_IN_BOTTOM_LEFT, 120, -12);
    lv_obj_set_event_cb(btn_B, cb_btntoAnaPageB);
    t_btnLabel = lv_label_create(btn_B, NULL);
    lv_label_set_text(t_btnLabel, "B");
    lv_btn_set_checkable(btn_B, true);

    lv_obj_t * btn_toSwitch = lv_btn_create(page_05Analogic, NULL);
    lv_obj_set_size(btn_toSwitch, 100, 35);
    lv_obj_set_event_cb(btn_toSwitch, cb_btntoSwitch);
    lv_obj_align(btn_toSwitch, page_05Analogic, LV_ALIGN_IN_BOTTOM_RIGHT, -10, -12);
    lv_obj_t *btnLabel_toSwitch = lv_label_create(btn_toSwitch, NULL);
    lv_label_set_text(btnLabel_toSwitch, "BACK");
}

void lvChange_page05_addPoint(uint16_t Aug, uint16_t Ail, uint16_t Bug, uint16_t Bil)
{
    char t_char[30];

    if((int)Aug>12000) Aug = 12000;
    if((int)Aug<1) Aug = 100;
    if((int)Ail>12000) Ail = 12000;
    if((int)Ail<1) Ail = 100;
    if((int)Bug>12000) Bug = 12000;
    if((int)Bug<1) Bug = 100;
    if((int)Bil>12000) Bil = 12000;
    if((int)Bil<1) Bil = 100;

    lv_chart_set_next(chart_A, serA_ug, Aug/100);
    lv_chart_set_next(chart_A, serA_il, Ail/100);
    lv_chart_set_next(chart_B, serB_ug, Bug/100);
    lv_chart_set_next(chart_B, serB_il, Bil/100);

    sprintf(t_char, "%d.%d %%", Aug/100, abs(Aug%100));
    lv_label_set_text(label_Aug, t_char);
    sprintf(t_char, "%d.%d %%", Ail/100, abs(Ail%100));
    lv_label_set_text(label_Ail, t_char);
    sprintf(t_char, "%d.%d %%", Bug/100, abs(Bug%100));
    lv_label_set_text(label_Bug, t_char);
    sprintf(t_char, "%d.%d %%", Bil/100, abs(Bil%100));
    lv_label_set_text(label_Bil, t_char);

}
