#include "exc_screen.h"
#include "lvgl/lvgl.h"
#include <stdio.h>

lv_obj_t *page_1,*page_2,*page_switch;

//*
//sendMode:0 no send; 1 send a bit; 2 send array
//*
struct _EXCWriteData{
    uint16_t addr;
    uint16_t data;
    uint16_t *dataArray;
    uint8_t sendArrayAmount;
    uint8_t sendMode;
    uint16_t beforeDelay;
    uint32_t AfterDelay;
};
struct _EXCWriteDataQueue{
    struct _EXCWriteData data[30];
    uint8_t headPoint;
    uint8_t counter;
};
struct _EXCWriteDataQueue EXCWriteDataQueue={
    .headPoint = 0,
    .counter = 0
};

static lv_obj_t* g_taSendMsg;
void lvAdd_taSendMsg(void)
{
    g_taSendMsg = lv_textarea_create(lv_scr_act(), NULL);
    lv_obj_set_size(g_taSendMsg, 200, 100);
    lv_obj_align(g_taSendMsg, NULL, LV_ALIGN_CENTER, 0, 0);
    lv_textarea_set_text(g_taSendMsg, "send Msg"); /*Set an initial text*/
    lv_obj_set_hidden(g_taSendMsg, true);
}

void ModbusAddWriteDataSingal(uint16_t addr, uint16_t data)
{
    uint8_t headPoint = EXCWriteDataQueue.headPoint;
    uint8_t counter = EXCWriteDataQueue.counter;
//    uint8_t newPoint = headPoint+counter-1;
    uint8_t nextPoint = headPoint+counter;
    if(counter>=30) return;
    if(nextPoint>=30) nextPoint-=30;
    EXCWriteDataQueue.data[nextPoint].addr = addr;
    EXCWriteDataQueue.data[nextPoint].data = data;
    EXCWriteDataQueue.data[nextPoint].dataArray = NULL;
    EXCWriteDataQueue.data[nextPoint].sendArrayAmount = 0;
    EXCWriteDataQueue.data[nextPoint].sendMode = 1;
    EXCWriteDataQueue.data[nextPoint].beforeDelay = 100;
    EXCWriteDataQueue.data[nextPoint].AfterDelay = 100;
    EXCWriteDataQueue.counter++;
}

static void ModbusSendWriteDataSingal(){
    if(EXCWriteDataQueue.counter <= 0) return;
    uint8_t headPoint = EXCWriteDataQueue.headPoint;
    uint16_t addr = EXCWriteDataQueue.data[headPoint].addr;
    uint16_t data = EXCWriteDataQueue.data[headPoint].data;
    //send handle
    lv_obj_set_hidden(g_taSendMsg, false);
    EXCWriteDataQueue.data[headPoint].sendMode = 0;
}

static void ModbusPopWriteDataSingal(){
    if(EXCWriteDataQueue.counter <= 0) return;
//    uint8_t headPoint = EXCWriteDataQueue.headPoint;
//    uint8_t counter = EXCWriteDataQueue.counter;
//    uint16_t addr = EXCWriteDataQueue.data[headPoint].addr;
//    uint16_t data = EXCWriteDataQueue.data[headPoint].data;
    if(EXCWriteDataQueue.headPoint == 29) EXCWriteDataQueue.headPoint = 0;
    else ++EXCWriteDataQueue.headPoint;
    --EXCWriteDataQueue.counter;
    lv_obj_set_hidden(g_taSendMsg, true);
}

void ModbusHandleWrite(uint8_t relayTime)
{
    if(EXCWriteDataQueue.counter <= 0) return;
    uint8_t headPoint = EXCWriteDataQueue.headPoint;
    if(EXCWriteDataQueue.data[headPoint].sendMode == 1)
        if(EXCWriteDataQueue.data[headPoint].beforeDelay<=relayTime)
            ModbusSendWriteDataSingal();
        else EXCWriteDataQueue.data[headPoint].beforeDelay-=relayTime;
    else if(EXCWriteDataQueue.data[headPoint].sendMode == 0)
        if(EXCWriteDataQueue.data[headPoint].AfterDelay<=relayTime)
            ModbusPopWriteDataSingal();
        else EXCWriteDataQueue.data[headPoint].AfterDelay-=relayTime;
    else;
}

//起励
//例程入口
void lvAdd_excScreen()
{
    lvAdd_pageSwitch();
    lvAdd_page_01firstPage();
    lvAdd_page_02Promotion();
    lvAdd_page_03OperationLock();
    lvAdd_page_05Analogic();
    lvAdd_page_06IO();
    lvAdd_taSendMsg();

//    lvChange_switchPage(page_switch);
    lvChange_switchPage(page_05Analogic);
}


void cb_btntoSwitch(lv_obj_t * btn, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED)
    {
        lvChange_switchPage(page_switch);
    }
}

void lv_obj_set_gaval(lv_obj_t * obj, lv_coord_t gaval)
{
    lv_gauge_set_value(obj, 0, gaval);
}

void lvChange_gaugeAnim(lv_obj_t *gauge, lv_anim_value_t value)
{
    lv_anim_t a;
    lv_anim_init(&a);
    /* MANDATORY SETTINGS
    *------------------*/
    /*Set the "animator" function*/
    lv_anim_set_exec_cb(&a, (lv_anim_exec_xcb_t) lv_obj_set_gaval);
    /*Set the "animator" function*/
    lv_anim_set_var(&a, gauge);
    /*Length of the animation [ms]*/
    lv_anim_set_time(&a, 300);
    /*Set start and end values. E.g. 0, 150*/
    lv_anim_set_values(&a, lv_gauge_get_value(gauge, 0), value);
    lv_anim_start(&a);
}
