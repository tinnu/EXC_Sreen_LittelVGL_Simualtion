#include "exc_screen.h"


static lv_obj_t * slider_label;
static lv_obj_t * btn_label;
lv_obj_t *gaugeUg,*gaugeIL,*gaugeP,*gaugeQ,
         *label_gaugeUg,*label_gaugeIL,*label_gaugeP,*label_gaugeQ,
        *led_auto, *led_inv, *led_cutin, *led_PSS,
        *bar_channalA, *bar_channalB;


static void cb_sliderTest(lv_obj_t * slider, lv_event_t event)
{
    if(event == LV_EVENT_VALUE_CHANGED) {
        static char buf[4]; /* max 3 bytes for number plus 1 null terminating byte */
        snprintf(buf, 4, "%u", lv_slider_get_value(slider));
        lv_label_set_text(slider_label, buf);
        /* INITIALIZE AN ANIMATION
        *-----------------------*/
        lv_anim_t a;
        lv_anim_init(&a);
        /* MANDATORY SETTINGS
        *------------------*/
        /*Set the "animator" function*/
        lv_anim_set_exec_cb(&a, (lv_anim_exec_xcb_t) lv_obj_set_gaval);
        /*Set the "animator" function*/
        lv_anim_set_var(&a, gaugeUg);
        /*Length of the animation [ms]*/
        lv_anim_set_time(&a, 300);
        /*Set start and end values. E.g. 0, 150*/
        lv_anim_set_values(&a, lv_gauge_get_value(gaugeUg, 0), lv_slider_get_value(slider)*2);
        lv_anim_start(&a);
    }
}

static void cb_btnPageMain2Promotion(lv_obj_t * btn, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED)
    {
        lvChange_switchPage(page_2);
    }
}

void lvAdd_page_01firstPage(void)
{
    page_1 = lv_page_create(lv_scr_act(), NULL);
    lv_obj_set_size(page_1, 480, 272);
    lv_obj_align(page_1, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, 0);
    lv_page_set_scrollbar_mode(page_1, LV_SCROLLBAR_MODE_DRAG);

    //Describe the color for the needles
    static lv_color_t needle_colors[3];
    needle_colors[0] = LV_COLOR_ORANGE;
    needle_colors[1] = lv_color_hex(0x44d1b6);
    needle_colors[2] = LV_COLOR_PURPLE;

    //1-仪表盘
    lv_coord_t t_gaugeSize = 125;
    lv_coord_t t_gaugeStep = 132;
    lv_coord_t t_gaugeLabelStep = 6;
    gaugeUg = lv_gauge_create(page_1, NULL);
    lv_gauge_set_range(gaugeUg,0,200);
    lv_gauge_set_critical_value(gaugeUg, 110);
    lv_gauge_set_scale(gaugeUg, 300, 21, 5);
    lv_gauge_set_needle_count(gaugeUg, 1, needle_colors);
    lv_obj_set_size(gaugeUg, t_gaugeSize, t_gaugeSize);
    lv_obj_align(gaugeUg, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 0);
    lv_obj_set_click(gaugeUg, false);
    label_gaugeUg = lv_label_create(page_1, NULL);
    lv_label_set_text(label_gaugeUg, "PT\t 0.00%");
    lv_obj_align(label_gaugeUg, gaugeUg, LV_ALIGN_OUT_BOTTOM_MID, 0,t_gaugeLabelStep);

    gaugeIL = lv_gauge_create(page_1, NULL);
    lv_gauge_set_range(gaugeIL,0,200);
    lv_gauge_set_critical_value(gaugeIL, 110);
    lv_gauge_set_scale(gaugeIL, 300, 21, 5);
    lv_gauge_set_needle_count(gaugeIL, 1, needle_colors);
    lv_obj_set_size(gaugeIL, t_gaugeSize, t_gaugeSize);
    lv_obj_align(gaugeIL, NULL, LV_ALIGN_IN_TOP_LEFT, t_gaugeStep, 0);
    lv_obj_set_click(gaugeIL, false);
    label_gaugeIL = lv_label_create(page_1, NULL);
    lv_label_set_text(label_gaugeIL, "IL\t 0.00%");
    lv_obj_align(label_gaugeIL, gaugeIL, LV_ALIGN_OUT_BOTTOM_MID, 0,t_gaugeLabelStep);

    gaugeP = lv_gauge_create(page_1, NULL);
    lv_gauge_set_range(gaugeP,0,200);
    lv_gauge_set_critical_value(gaugeP, 110);
    lv_gauge_set_scale(gaugeP, 300, 21, 5);
    lv_gauge_set_needle_count(gaugeP, 1, needle_colors);
    lv_obj_set_size(gaugeP, t_gaugeSize, t_gaugeSize);
    lv_obj_align(gaugeP, NULL, LV_ALIGN_IN_TOP_LEFT, t_gaugeStep*2, 0);
    lv_obj_set_click(gaugeP, false);
    label_gaugeP = lv_label_create(page_1, NULL);
    lv_label_set_text(label_gaugeP, "P\t 0.00%");
    lv_obj_align(label_gaugeP, gaugeP, LV_ALIGN_OUT_BOTTOM_MID, 0,t_gaugeLabelStep);

    gaugeQ = lv_gauge_create(page_1, NULL);
    lv_gauge_set_range(gaugeQ,0,200);
    lv_gauge_set_critical_value(gaugeQ, 110);
    lv_gauge_set_scale(gaugeQ, 300, 21, 5);
    lv_gauge_set_needle_count(gaugeQ, 1, needle_colors);
    lv_obj_set_size(gaugeQ, t_gaugeSize, t_gaugeSize);
    lv_obj_align(gaugeQ, NULL, LV_ALIGN_IN_TOP_LEFT, t_gaugeStep*3, 0);
    lv_obj_set_click(gaugeQ, false);
    label_gaugeQ = lv_label_create(page_1, NULL);
    lv_label_set_text(label_gaugeQ, "Q\t 0.00%");
    lv_obj_align(label_gaugeQ, gaugeQ, LV_ALIGN_OUT_BOTTOM_MID, 0,t_gaugeLabelStep);

    //2-状态指示灯
    lv_coord_t t_ledSize = 15;
    lv_coord_t t_ledStep = 10;
    led_auto = lv_led_create(page_1, NULL);
    lv_obj_set_size(led_auto, t_ledSize,t_ledSize);
    lv_obj_align(led_auto, gaugeUg, LV_ALIGN_OUT_BOTTOM_LEFT, 10, 25+t_ledStep);
    lv_led_off(led_auto);
    lv_obj_t *led1_label = lv_label_create(page_1, NULL);
    lv_label_set_text(led1_label, "AUTO");
    lv_obj_align(led1_label, led_auto, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    led_inv = lv_led_create(page_1, NULL);
    lv_obj_set_size(led_inv, t_ledSize,t_ledSize);
    lv_obj_align(led_inv, led_auto, LV_ALIGN_OUT_BOTTOM_LEFT, 0, t_ledStep);
    lv_led_off(led_inv);
    led1_label = lv_label_create(page_1, NULL);
    lv_label_set_text(led1_label, "INV");
    lv_obj_align(led1_label, led_inv, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    led_cutin = lv_led_create(page_1, NULL);
    lv_obj_set_size(led_cutin, t_ledSize,t_ledSize);
    lv_obj_align(led_cutin, led_inv, LV_ALIGN_OUT_BOTTOM_LEFT, 0, t_ledStep);
    lv_led_off(led_cutin);
    led1_label = lv_label_create(page_1, NULL);
    lv_label_set_text(led1_label, "CUTIN");
    lv_obj_align(led1_label, led_cutin, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    led_PSS = lv_led_create(page_1, NULL);
    lv_obj_set_size(led_PSS, t_ledSize,t_ledSize);
    lv_obj_align(led_PSS, led_cutin, LV_ALIGN_OUT_BOTTOM_LEFT, 0, t_ledStep);
    lv_led_off(led_PSS);
    led1_label = lv_label_create(page_1, NULL);
    lv_label_set_text(led1_label, "PSS");
    lv_obj_align(led1_label, led_PSS, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

    //3-bar
    bar_channalA = lv_bar_create(page_1, NULL);
    lv_obj_set_size(bar_channalA, 200, 15);
    lv_obj_align(bar_channalA, led_auto, LV_ALIGN_OUT_RIGHT_MID, 120, 10);
    lv_bar_set_anim_time(bar_channalA, 500);
    led1_label = lv_label_create(page_1, NULL);
    lv_label_set_text(led1_label, "A");
    lv_obj_align(led1_label, bar_channalA, LV_ALIGN_OUT_LEFT_MID, -10, 0);

    bar_channalB = lv_bar_create(page_1, NULL);
    lv_obj_set_size(bar_channalB, 200, 15);
    lv_obj_align(bar_channalB, bar_channalA, LV_ALIGN_OUT_BOTTOM_MID, 0, 30);
    lv_bar_set_anim_time(bar_channalB, 500);
    led1_label = lv_label_create(page_1, NULL);
    lv_label_set_text(led1_label, "B");
    lv_obj_align(led1_label, bar_channalB, LV_ALIGN_OUT_LEFT_MID, -10, 0);

    //3-跳转按键
    lv_obj_t * btn_PageMain2Promotion = lv_btn_create(page_1, NULL);     //Add a button the current screen
    lv_obj_set_size(btn_PageMain2Promotion, 100, 35);                          //Set its size
    lv_obj_set_event_cb(btn_PageMain2Promotion, cb_btnPageMain2Promotion);                 //Assign a callback to the button
    lv_obj_align(btn_PageMain2Promotion, gaugeQ, LV_ALIGN_OUT_BOTTOM_RIGHT, 0, 40);
    lv_obj_t *btnLabel_PageMain2Promotion = lv_label_create(btn_PageMain2Promotion, NULL);
    lv_label_set_text(btnLabel_PageMain2Promotion, "Promotion");

    lv_obj_t * btn_toSwitch = lv_btn_create(page_1, NULL);     //Add a button the current screen
    lv_obj_set_size(btn_toSwitch, 100, 35);                          //Set its size
    lv_obj_set_event_cb(btn_toSwitch, cb_btntoSwitch);                 //Assign a callback to the button
    lv_obj_align(btn_toSwitch, btn_PageMain2Promotion, LV_ALIGN_OUT_BOTTOM_MID, 0, 10);
    lv_obj_t *btnLabel_toSwitch = lv_label_create(btn_toSwitch, NULL);
    lv_label_set_text(btnLabel_toSwitch, "Swich Page");

    //4-测试滑条
    lv_obj_t * slider = lv_slider_create(page_1, NULL);
    lv_obj_set_width(slider, LV_DPI * 2);
    lv_obj_align(slider, bar_channalA, LV_ALIGN_OUT_BOTTOM_MID, 0, 20);
    lv_obj_set_event_cb(slider, cb_sliderTest);
    lv_slider_set_range(slider, 0, 100);
    // Create a label below the slider
    slider_label = lv_label_create(page_1, NULL);
    lv_label_set_text(slider_label, "0");
    lv_obj_set_auto_realign(slider_label, true);
    lv_obj_align(slider_label, slider, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);

}

void lvChange_pagemaingauge(uint16_t value1, uint16_t value2, uint16_t value3, uint16_t value4)
{
    static uint16_t t1=0,t2=0,t3=0,t4=0;
    char t_char[30];

    if(t1 != value1)
    {
        sprintf(t_char, "Ug\t %d.%d %%", value1/100, value1%100);
        lv_label_set_text(label_gaugeUg, t_char);
        lvChange_gaugeAnim(gaugeUg, (double)value1/100);
    }
    if(t2 != value2)
    {
        sprintf(t_char, "IL\t %.2f %%", (double)value2/100);
        lv_label_set_text(label_gaugeIL, t_char);
        lvChange_gaugeAnim(gaugeIL, (double)value2/100);
    }
    if(t3 != value3)
    {
        sprintf(t_char, "P\t %.2f %%", (double)value3/100);
        lv_label_set_text(label_gaugeP, t_char);
        lvChange_gaugeAnim(gaugeP, (double)value3/100);
    }
    if(t4 != value4)
    {
        sprintf(t_char, "Q\t %.2f %%", (double)value4/100);
        lv_label_set_text(label_gaugeQ, t_char);
        lvChange_gaugeAnim(gaugeQ, (double)value4/100);
    }
    t1=value1;
    t2=value2;
    t3=value3;
    t4=value4;
}



void lvChange_pagemainBar(uint16_t value1, uint16_t value2)
{
    static uint16_t t1=0,t2=0;
    if(t1 != value1)
        lv_bar_set_value(bar_channalA, (double)value1/100, LV_ANIM_ON);
    if(t2 != value2)
        lv_bar_set_value(bar_channalB, (double)value2/100, LV_ANIM_ON);
    t1=value1;
    t2=value2;
}
